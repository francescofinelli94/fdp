#++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#      PLOT ONE 
# plots energy densities: kinetic, electric, internal,
# magnetic. superplots electron, ion and barycenter 
# fluid velocities as streamplots, electric and magnetic
# fields as arrow plots and highlights zones of high 
# current density by dotting them
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++


#----choose-labels-------------------------------------------
tar_labs = [
  ['Ki_'+t,'Ke_'+t,'K_'+t,'enE_'+t],
  ['Ui_'+t,'Ue_'+t,'U_'+t,'enB_'+t]]

#----choose-variables-to-be-plotted-and-levels---------------
tar_vars_x = [
  'ui_x_'+t,'ue_x_'+t,'u_x_'+t,
  'ui_x_'+t,'ue_x_'+t,'u_x_'+t]
tar_vars_y = [
  'ui_y_'+t,'ue_y_'+t,'u_y_'+t,
  'ui_y_'+t,'ue_y_'+t,'u_y_'+t]
tar_varf = [
  'Ki_'+t,'Ke_'+t,'K_'+t,'enE_'+t,
  'Ui_'+t,'Ue_'+t,'U_'+t,'enB_'+t]

tar_varc = ['|J|_'+t,'|J|_'+t]
tar_varq_x = ['E_x_'+t,'B_x_'+t]
tar_varq_y = ['E_y_'+t,'B_y_'+t]  


denstr = [2,2]
denqui = [5,5]

stylec = ['r',[None,'..','....']]
mymax = np.log10(np.percentile(alldata.data['|J|_'+t][ranx[0]:ranx[1],rany[0]:rany[1],cut_z],99.95))
mymin = np.log10(np.percentile(alldata.data['|J|_'+t][ranx[0]:ranx[1],rany[0]:rany[1],cut_z],0.005)) #max(np.min(alldata.data['|J|_'+t]),1e-6))
mylvc = np.logspace(mymin,mymax,10)[-2:]

mylvf = np.linspace(0.0,1.0,10,endpoint=True)

#-----generate-lists-of-subplots--------------------------
my_fig = alldata.draw_canvas(tar_labs,'cont')
re_fig = [my_fig[0],my_fig[1],my_fig[2],my_fig[4],my_fig[5],my_fig[6]]
er_fig = [my_fig[3],my_fig[7]]

#-----plot-all-quantities----------------------------------
alldata.draw_spotted(my_fig,tar_varf,levbar=True,range_x=ranx,range_y=rany)#,levels=mylvf)
alldata.draw_streams(re_fig,tar_vars_x,tar_vars_y,density=denstr,range_x=ranx,range_y=rany)
alldata.draw_spotted(er_fig,tar_varc,levels=mylvc,styles=stylec,range_x=ranx,range_y=rany,alpha=0.01)
alldata.draw_quivers(er_fig,tar_varq_x,tar_varq_y,range_x=ranx,range_y=rany,cut_z=cut_z,density=denqui,styles=['w',1.],alpha=0.6)
#alldata.draw_streams(er_fig,tar_varq_x,tar_varq_y,range_x=ranx,range_y=rany,density=denstr)

if print_me: plt.savefig(print_address+alldata.fibo_name+'_test_plot_one.png',format='png')
if show_me: plt.show()
